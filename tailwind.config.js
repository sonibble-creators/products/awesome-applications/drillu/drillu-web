const plugin = require("tailwindcss/plugin");

/**
 * # Tailwind Css configuration file
 * configure some of the css style element and class
 * add colors, typography, font-family, etc
 */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    fontFamily: {
      body: ["DM Sans", "sans-serif"],
      heading: ["Poppins", "sans-serif"],
      writing: ["Leckerli One", "cursive"]
    },
    extend: {
      primary: {
        indigo: {
          500: "#6759FF"
        },
        cyan: "#5FD9FF",
        purple: "#7923FF"
      }
    },
  },
  plugins: [
    plugin(function ({ addUtilities }) {
      addUtilities({
        ".no-scrollbar": {
          "-ms-overflow-style": "none",
          "scrollbar-width": "none",
        },
        ".no-scrollbar::-webkit-scrollbar": {
          "display": "none",
        }
      })
    }),
  ],
}