import type { LinksFunction, MetaFunction } from "@remix-run/node";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
} from "@remix-run/react";
import appStyle from "~/styles/app.css";

export const meta: MetaFunction = () => ({
  charset: "utf-8",
  title: "Drillu - Free learning platform for developers, designer",
  viewport: "width=device-width,initial-scale=1",
  description: "Drillu is a free learning platform for developers, designers",
});

export const links: LinksFunction = () => [
  { rel: "stylesheer", href: appStyle },
  { rel: "icon", href: "/favicon.ico" },
];

export default function App() {
  return (
    <html lang="en">
      <head>
        <Meta />
        <Links />
      </head>
      <body className="font-body font-normal leading-normal text-black text-base">
        <Outlet />
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
}
